package com.codelabs.wessh.codelabsprimeiro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void toastMe(View view){
        Toast mytoast = Toast.makeText(this, "Fala Zé Ruela!", Toast.LENGTH_SHORT);
        mytoast.show();
    }

    public void contar(View view){
        TextView contarum = (TextView) findViewById(R.id.btView);

        String contadorum = contarum.getText().toString();

        Integer contar = Integer.parseInt(contadorum);
        contar++;

        if(contar == 101) {
            Toast opa = Toast.makeText(this, "Só vamos até 100 beleza!", Toast.LENGTH_SHORT);
            opa.show();
        }else{
            contarum.setText(contar.toString());
        }

    }
    public void zerar(View view){
        TextView zerar = findViewById(R.id.btView);

        zerar.setText("0");
    }

    public void  aleatorio(View view){
        TextView aleat = findViewById(R.id.btView);

        String tv = aleat.getText().toString();

        double numero = Math.random() * 100;
        int valorAleatorio = (int) Math.round(numero);
        Integer valor = valorAleatorio;

        aleat.setText(valor.toString());

        Integer tvc = Integer.parseInt(tv);

        if(tvc == 100){
            Toast opa = Toast.makeText(this, "Só até 100 amigo!", Toast.LENGTH_SHORT);
            opa.show();
        }

    }
}
